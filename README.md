## Botocss

Pronounced 'botox'. Injects CSS into your HTML markup for sending via email. Guaranteed to take you back 10 years.

### Usage

1. Include Botocss in your project, either by downloading the JAR or using the Maven dependency:

        <dependency>
            <groupId>com.atlassian.botocss</groupId>
            <artifactId>botocss-core</artifactId>
            <version>5.2</version>
        </dependency>

    This dependency is hosted in the [atlassian-contrib](https://maven.atlassian.com/content/repositories/atlassian-contrib/) Maven repository.

2. Use the `Botocss.inject()` method to inject CSS into your markup.

        // apply external stylesheet
        String markup = "<html><body><p>Hello, world!</p></body></html>";
        String styles = "p { margin: 10px; }";
        String output = Botocss.inject(markup, styles);

        // Botocss also applies internal styles
        String markupWithStyles = "<html>" +
            "<head><style>p { margin: 10px; }</style></head>" +
            "<body><p>Hello, world!</p></body>" +
            "</html>";
        String output = Botocss.inject(markup);

If you are processing a large number of HTML documents with the same stylesheet, Botocss provides a method to parse your stylesheet just once and reuse it:

        // example of reusing stylesheets
        BotocssStyles styles = Botocss.parse(css1, css2);
        for (String input : inputs) {
            output.add(Botocss.inject(html, styles));
        }

By default, Botocss will use output settings optimized for production use. This means there will be no linefeeds and no whitespace indentation between tags. If you want to output in a more human-readable fashion, pass `DocumentFunctions.PRETTY_PRINT` to the three-argument version of `Botocss.inject()`.

### Notes

Styles included in the document take precedence over those that appear in external stylesheets, as they would in CSS. Style blocks in the markup are left there for user agents that support them.

Styles are appended in order to the `@style` attribute on any matching elements. Botocss does not currently remove redundant styles, it relies on the user agent to disregard them.

Most common CSS selectors are supported:

* descendent, child, immediate sibling element selectors (`a b`, `a > b`, `a + b`)
* id, class, attribute selectors (`#id`, `.class`, `a[href]`),
* basic pseudo-selectors (`:first-child`, `:last-child`).
* [structural CSS3 selectors](http://www.w3.org/TR/css3-selectors/#structural-pseudos) (`:nth-child`, `nth-of-type`, ...)
* advanced attribute element (`div[data=foobar]`, `a[href^=mailto]` etc)

Unsupported selectors are ignored during processing. Known unsupported selectors include:

* pseudo-selectors which can't be applied via inline styles (`:hover`, `:active`)
* some pseudo-selectors (`:before`, `:after`)

You can enable INFO or DEBUG logging for detailed information about the processing. The log category name is 'com.atlassian.botocss'.

**Specificity is not currently respected.** The order that styles appear in the stylesheet or document are the order they are applied. Any `!important` declarations are not respected either.

The HTML parsing and manipulation requires storing the HTML document in memory. Be aware that loading very large HTML documents may require a significant amount of memory. You probably don't want to send a massive chunk of HTML via email anyway.

### Dependencies

Botocss depends the following libraries at runtime, as noted in the project's POM:

* [jStyleparser](http://cssbox.sourceforge.net/jstyleparser/) - LGPL v3 license
* [jsoup](http://jsoup.org/) - HTML parsing and manipulation - MIT license
* slf4j-api (and a compatible sfl4j implementation at runtime) - MIT license
* Java 8 or later.

The "Botocss with jStyleParser bundled" (`com.atlassian.botocss:botocss`) artifact includes a bundled copy of jStyleparser, for backwards compatibility with clients who do not want a dependency on Atlassian's 3rdparty Maven repository.

Botocss also requires a JAXP (XML parsing) implementation like Xerces on the classpath. This is usually provided by the JDK, but you might require the library at runtime if running in certain web application containers.

### Releasing

Tested with Maven 3.0.5:

    mvn release:prepare
    mvn release:perform

Preferably release from the Bamboo plan located at https://engservices-bamboo.internal.atlassian.com/browse/ATLASSIANBOTOCSS

### License

Botocss is licensed under the BSD license as follows. Note that the 'Botocss with jStyleParser bundled' artifact contains the jStyleparser library which is LGPL-licensed as listed above.

> Copyright (c) 2011-2014 Atlassian. All rights reserved.

> Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

> * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
> * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
> * The names of contributors may not be used to endorse or promote products derived from this software without specific prior written permission.

> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.